import logo from './logo.svg';
import './App.css';
import Helo from './created/Helo';
import Top from './created/Top';
import ApiTest from './created/ApiTest';

function App() {
  return (
    <div className="App">
     <Helo test = {" - 5GI"}></Helo>
     <Top btnText={"Let's go RED Baby"}></Top>
     <p>Future Ingénieur EHEI</p>
     <ApiTest></ApiTest>
     {/*
  <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
     */} 
      
    </div>
    
  );
}

export default App;
