import React, {useState } from 'react'

export const ApiTest = () => {

    const [tabledata, setTabledata] = useState([]);

    var test = ()=>{
        fetch('https://jsonplaceholder.typicode.com/todos')
        .then(response => response.json())
        .then(json => setTabledata(json))
    }

    
       test();
     
       const styles = {
        border: '1px solid rgba(0, 0, 0, 0.05)',
        borderRadius : '20px',
        backgroundColor : '#d9d9d9',
        padding : '10px',
        margin : '10px'
   };



   
    return (
        <div >
            
            {tabledata.map((doto) => (<h2 style={styles} key={doto.id}>{doto.title}</h2>))}
          
        </div>
    )
}

export default ApiTest
